  .. _selectors:

 .. role:: important

<BoxROI /\>
===========
Select all the points, edges, triangles, quads, tetrahedrons or hexahedrons contained inside a given box.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 5 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Direction
      - Format
      - Description
    * - box
      - input
      - xmin,ymin,zmin, xmax,ymax,zmax
      - List of boxes defined by xmin,ymin,zmin, xmax,ymax,zmax
    * - drawBoxes
      - input
      - bool
      - If true, the boxes will be displayed.
    * - position
      - input
      - path
      - Rest position coordinates of the degrees of freedom. If empty the positions from a MechanicalObject then a MeshLoader are searched in the current context. If none are found the parent's context is searched for MechanicalObject.
    * - edges
      - input
      - path
      - Path to a set of edge indices.
    * - triangles
      - input
      - path
      - Path to a set of triangle indices.
    * - quad
      - input
      - path
      - Path to a set of quad indices.
    * - tetrahedra
      - input
      - path
      - Path to a set of tetrahedron indices.
    * - hexahedra
      - input
      - path
      - Path to a set of hexahedron indices.
    * - indices
      - output
      - [i0 i1 i2 ...]
      - Indices of the nodes contained in the ROI.
    * - pointsInROI
      - output
      - [x0 x1 x2 ...]
      - Positions of the nodes contained in the ROI.
    * - edgesInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of edges contained in the ROI where two consecutive indices form an edge.
    * - trianglesInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of triangles contained in the ROI where three consecutive indices form a triangle.
    * - quadsInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of quads contained in the ROI where four consecutive indices form a quad.
    * - tetrahedraInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of tetrahedrons contained in the ROI where four consecutive indices form a tetrahedron.
    * - hexahedraInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of hexahedrons contained in the ROI where eight consecutive indices form an hexahedron.

<SphereROI /\>
==============
Select all the points, edges, triangles, quads, tetrahedrons or hexahedrons contained inside a given sphere.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 5 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Direction
      - Format
      - Description
    * - centers
      - input
      - c1 c2 c3 ...
      - List of sphere center points defined by x y z
    * - radii
      - input
      - r1 r2 r3 ...
      - List of spere radius
    * - position
      - input
      - path
      - Rest position coordinates of the degrees of freedom. If empty the positions from a MechanicalObject then a MeshLoader are searched in the current context. If none are found the parent's context is searched for MechanicalObject.
    * - edges
      - input
      - path
      - Path to a set of edge indices.
    * - triangles
      - input
      - path
      - Path to a set of triangle indices.
    * - quad
      - input
      - path
      - Path to a set of quad indices.
    * - tetrahedra
      - input
      - path
      - Path to a set of tetrahedron indices.
    * - hexahedra
      - input
      - path
      - Path to a set of hexahedron indices.
    * - pointsInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices contained in the ROI.
    * - edgesInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of edges contained in the ROI where two consecutive indices form an edge.
    * - trianglesInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of triangles contained in the ROI where three consecutive indices form a triangle.
    * - quadsInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of quads contained in the ROI where four consecutive indices form a quad.
    * - tetrahedraInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of tetrahedrons contained in the ROI where four consecutive indices form a tetrahedron.
    * - hexahedraInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices of hexahedrons contained in the ROI where eight consecutive indices form an hexahedron.

<ProximityROI /\>
=================
Select all the points contained close to a give set of positions.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 5 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Direction
      - Format
      - Description
    * - centers
      - input
      - c1 c2 c3 ...
      - List of sphere center points defined by x y z
    * - radii
      - input
      - r1 r2 r3 ...
      - List of spere radius
    * - N
      - input
      - int
      - Maximum number of points to select in each region
    * - position
      - input
      - path
      - Rest position coordinates of the degrees of freedom. If empty the positions from a MechanicalObject then a MeshLoader are searched in the current context. If none are found the parent's context is searched for MechanicalObject.
    * - pointsInROI
      - output
      - [i0 i1 i2 ...]
      - Point indices contained in the ROI.
    * - distance
      - output
      - [d1 d2 d3 ...]
      - Distance of each output point from their closer center