 .. _collisions:

 .. role:: important

In all SOFA simulations, the collision phase between different simulated objects is done separately from the physics simulation. During a time step, the new position of a mechanical object will be obtained in a physical way by the system solver without taking into account the collisions. Once these new positions are computed, the collision detection phase will start and extract all the collisions model that violate the interpenetration constraints. These constraints will then be enforced with an interative solving process until all objects of the simulation comply.

Example of a collision scene:

.. code-block:: xml
    :linenos:

    <Node name="root" dt="0.01" gravity="0 0 -9">
        <VisualStyle displayFlags="showCollisionModels hideVisualModels" />
        <DefaultAnimationLoop />
        <CollisionPipeline verbose="0" draw="0"/>
        <BruteForceDetection name="N2"/>
        <MinProximityIntersection name="Proximity" alarmDistance="0.5" contactDistance="0.25"/>
        <CollisionResponse name="Response" response="default"/>

        <Node name="Sphere">
            <EulerImplicitSolver rayleighStiffness="0"/>
            <CGLinearSolver iterations="200" tolerance="1e-06" threshold="1e-06"/>
            <MechanicalObject template="Rigid" name="sphere_mo" position="5 5 10    0 0 0 1" showObject="1" showObjectScale="0.5" />
            <UniformMass totalMass="1" />
            <Sphere name="Floor" radius="1" contactStiffness="100" />
        </Node>


        <Node name="Floor">
            <MechanicalObject template="Vec3d" name="floor_mo" position="0 0 0     10 0 0     10 10 0    0 10 0" />
            <TriangleSetTopologyContainer points="0 1 2 3" edges="0 1    1 3    3 0    2 3   2  1" triangles="0 1 3    1 2 3"/>
            <Point simulated="0" moving="0" />
            <Line simulated="0" moving="0" />
            <Triangle simulated="1" moving="1" />
        </Node>
    </Node>


A collision model is needed to represent a topology that must comply to a non interpenetration constraint with itself, or with anoter collision model. The following collision models are available:

<Point /\>
===============
Indicates that the positions of a mechanical object must comply to the collision constraints, hence they may not penetrate any other element that is represented by a collision model.

:important:`Requires a mechanical object with template="Vec3d".`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - active
      - bool (default: true)
      - Flag indicating if this collision model is active and should be included in default collision detections.
    * - moving
      - bool (default: true)
      - Flag indicating if this object is changing position between iterations.
    * - simulated
      - bool (default: true)
      - Flag indicating if this object is controlled by a simulation.
    * - selfCollision
      - bool (default: false)
      - Flag indication if the object can self collide.
    * - proximity
      - float (default: 0.0)
      - Distance to the actual (visual) surface.
    * - contactStiffness
      - float (default: 10.0)
      - Contact stiffness :math:`k` used in penalty method. The contact force applied will be :math:`F_c = kd` where :math:`d` is the penetration distance.
    * - group
      - string
      - IDs of the groups containing this model. No collision can occur between collision models included in a common group (e.g. allowing the same object to have multiple collision models).

.. code-block:: xml
	:linenos:

	<MechanicalObject  template="Vec3f"  position="0 0 0 "/>
	<Point active="true" moving="true" simulated="true" />

<Line /\>
==============
Indicates that line elements of a topology must comply to the collision constraints, hence they may not penetrate any other element that is represented by a collision model.

:important:`Requires a mechanical object with template="Vec3d".`
:important:`Requires a topology container.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - active
      - bool (default: true)
      - Flag indicating if this collision model is active and should be included in default collision detections.
    * - moving
      - bool (default: true)
      - Flag indicating if this object is changing position between iterations.
    * - simulated
      - bool (default: true)
      - Flag indicating if this object is controlled by a simulation.
    * - selfCollision
      - bool (default: false)
      - Flag indication if the object can self collide.
    * - proximity
      - float (default: 0.0)
      - Distance to the actual (visual) surface.
    * - contactStiffness
      - float (default: 10.0)
      - Contact stiffness :math:`k` used in penalty method. The contact force applied will be :math:`F_c = kd` where :math:`d` is the penetration distance.
    * - group
      - string
      - IDs of the groups containing this model. No collision can occur between collision models included in a common group (e.g. allowing the same object to have multiple collision models).

.. code-block:: xml
	:linenos:

	<MechanicalObject  template="Vec3f"  position="0 0 0     10 0 0"/>
	<EdgeSetTopologyContainer points="0 1" edges="0 1"/>
	<Point active="true" moving="true" simulated="true" />
	<Line active="true" moving="true" simulated="true" />

<Triangle /\>
==================
Indicates that triangle elements of a topology must comply to the collision constraints, hence they may not penetrate any other element that is represented by a collision model.

:important:`Requires a mechanical object with template="Vec3d".`
:important:`Requires a topology container.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - active
      - bool (default: true)
      - Flag indicating if this collision model is active and should be included in default collision detections.
    * - moving
      - bool (default: true)
      - Flag indicating if this object is changing position between iterations.
    * - simulated
      - bool (default: true)
      - Flag indicating if this object is controlled by a simulation.
    * - selfCollision
      - bool (default: false)
      - Flag indication if the object can self collide.
    * - proximity
      - float (default: 0.0)
      - Distance to the actual (visual) surface.
    * - contactStiffness
      - float (default: 10.0)
      - Contact stiffness :math:`k` used in penalty method. The contact force applied will be :math:`F_c = kd` where :math:`d` is the penetration distance.
    * - group
      - string
      - IDs of the groups containing this model. No collision can occur between collision models included in a common group (e.g. allowing the same object to have multiple collision models).

.. code-block:: xml
	:linenos:

	<MechanicalObject  template="Vec3f"  position="0 0 0     10 0 0    10 0 0"/>
	<TriangleSetTopologyContainer points="0 1 2" edges="0 1   1 2    2 0" triangles="0 1 2"/>
	<Point active="true" moving="true" simulated="true" />
	<Line active="true" moving="true" simulated="true" />
	<Triangle active="true" moving="true" simulated="true" />

<Sphere /\>
================
Indicates that a sphere around each positions of a mechanical object must comply to the collision constraints, hence they may not penetrate any other element that is represented by a collision model.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - radius
      - float
      - Radius around each positions of the mechanical object that must comply to the collision constraints.
    * - active
      - bool (default: true)
      - Flag indicating if this collision model is active and should be included in default collision detections.
    * - moving
      - bool (default: true)
      - Flag indicating if this object is changing position between iterations.
    * - simulated
      - bool (default: true)
      - Flag indicating if this object is controlled by a simulation.
    * - selfCollision
      - bool (default: false)
      - Flag indication if the object can self collide.
    * - proximity
      - float (default: 0.0)
      - Distance to the actual (visual) surface.
    * - contactStiffness
      - float (default: 10.0)
      - Contact stiffness :math:`k` used in penalty method. The contact force applied will be :math:`F_c = kd` where :math:`d` is the penetration distance.
    * - group
      - string
      - IDs of the groups containing this model. No collision can occur between collision models included in a common group (e.g. allowing the same object to have multiple collision models).

.. code-block:: xml
	:linenos:

	<MechanicalObject  template="Rigid"  position="0 0 0"/>
	<Sphere radius="10"/>
