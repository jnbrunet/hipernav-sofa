<CGLinearSolver /\>
===================
Linear solver using the conjugate gradient iterative algorithm. The solver will stop when the specified thresholds are reached or when it hit the maximum number of iterations.

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `iterations`
      - int (default 25)
      - Maximum number of iterations of the Conjugate Gradient solution
    * - `tolerance`
      - float (default 1e-5)
      - Desired accuracy of the Conjugate Gradient solution (ratio of current residual norm over initial residual norm)
    * - `threshold`
      - float (default 1e-5)
      - Minimum value of the denominator in the conjugate Gradient solution"
