  .. _system_solvers:

 .. role:: important

<EulerImplicitSolver /\>
========================

Tries to solve the :math:`Ma + Ku = F` equation with a semi-implicit time integration scheme.

:important:`Requires a linear solver`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `rayleighStiffness`
      - float (default 0)
      - Rayleigh damping coefficient related to stiffness, > 0
    * - `rayleighMass`
      - float (default 0)
      - Rayleigh damping coefficient related to mass, > 0
    * - `vdamping`
      - float (default 0)
      - Velocity decay coefficient (no decay if null)

<EulerExplicitSolver /\>
========================
Tries to solve the :math:`Ma + Ku = F` equation with an explicit time integration scheme.

<StaticSolver /\>
=================
Tries to solve the :math:`Ku = F` equation (no time or mass term).

:important:`Requires a linear solver`