.. SOFA Framework documentation master file, created by
   sphinx-quickstart on Sat Dec  8 11:46:55 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HiPerNav SOFA lab!
=============================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

In this lab, you will be introduced to the world of deformable simulations with the help of the open source multiphysics SOFA framework.
You can use this website as a quick lookup documentation. Keep in mind that this document was made only for the lab, and a more complete 
documentation can be found at the official SOFA website:  https://www.sofa-framework.org/community/doc/.

This course will be divided into a series of exercises. At the end of this course, you should be comfortable with the basis of animation and 
linear elasticity theory.

Installing SOFA
---------------

SOFA Framework is a tool developed by researchers at INRIA aimed principally to interactive simulations in a medical context. You can get 
the current stable version by visiting https://www.sofa-framework.org/download.

Once it is installed, launch the application "runSofa" to make sure everything works fine.

For Linux, the "runSofa" executable will be located in the extracted folder, into the directory "bin".

For Mac, the "runSofa" executable will be located in the folder "/Applications/runSofa.app/Contents/MacOS".

Mac installation mirror link : https://drive.google.com/open?id=1gx3n4XK9KhDgKufel_lrylTVcKaaZQjf

Linux (glib2.23) installation mirror link: https://drive.google.com/open?id=134QBqnSymhPiqdlZ9pvUgyf-W-Q5zlql

Documentation
-------------

Throughout the following exercises, you can refer to the `SOFA training presentation <https://drive.google.com/open?id=1QH8e_go3-oEChCBlyu_Dlsyg33ypu1jqbRXKr8dhRPI>`_ and its related `scene files <_static/SofaTrainingScene-v2.zip>`_. You can also look at the numerous scene files in your SOFA installation directory, under the "examples" folder. You can use this website for a quick lookout of the most used SOFA components. Finally, the `official SOFA documentation <https://www.sofa-framework.org/community/doc/>`_ also contains a lot of useful information.

Exercise #1
-----------

For this exercise, create a simple scene that has two colliding objects: a ball and a floor. The ball is allowed to move, and should fall under the gravity directly into the center of the floor. You can refer to 
the documentation on :ref:`collisions <collisions>` to model the objects. You can also refer to the documentation on :ref:`system solvers <system_solvers>` and :ref:`mechanical states <mechanical_containers>` to animate the ball.

:note: You can build the entire scene without having to load a single mesh.
:note: Make sure to display the collision models by activating the flag "Collision Models" in the runSofa application under the "View" tab.

.. raw:: html

     <video controls autoplay muted loop src="_static/videos/ex1.mp4" width="620"></video> 

|

.. container:: toggle

    .. container:: header

        **Show/Hide solution**

    .. code-block:: xml
       :linenos:

        <Node name="root" dt="0.01" gravity="0 0 -9">
            <VisualStyle displayFlags="showCollisionModels hideVisualModels" />
            <CollisionPipeline verbose="0" depth="10" draw="0" />
            <BruteForceDetection name="N2" />
            <MinProximityIntersection name="Proximity" alarmDistance="0.5" contactDistance="0.25" />
            <CollisionResponse name="Response" response="default" />

            <Node name="Sphere">
                <EulerImplicitSolver rayleighStiffness="0"/>
                <CGLinearSolver iterations="200" tolerance="1e-06" threshold="1e-06"/>
                <MechanicalObject template="Rigid" name="sphere_mo" position="5 5 10    0 0 0 1" showObject="1" showObjectScale="0.5" />
                <UniformMass totalMass="100" />
                <Sphere name="Floor" radius="1" contactStiffness="100" />
            </Node>

            <Node name="Floor">
                <MechanicalObject template="Vec3d" name="floor_mo" position="0 0 0     10 0 0     10 10 0    0 10 0" />
                <TriangleSetTopologyContainer points="0 1 2 3" edges="0 1    1 3    3 0    2 3   2  1" triangles="0 1 3    1 2 3"/>
                <Point simulated="0" moving="0" contactStiffness="1000" />
                <Line simulated="0" moving="0" contactStiffness="1000" />
                <Triangle simulated="0" moving="0" contactStiffness="1000" />
            </Node>
        </Node>

|

Exercise #2
-----------

Change the previous scene to **add a deformable liver** between the ball and the floor. The volumetric mesh (with the set of tetrahedrons) of the liver is available in the file "mesh/liver.msh". Use a :ref:`mesh loader <mesh_loaders>` to load it. Use the appropriate :ref:`force field <forcefields>` for the elastic behavior of the liver.

.. raw:: html

     <video controls autoplay muted loop src="_static/videos/ex3.mp4" width="620"></video> 

.. toctree::
   :caption: Mechanical state
   :maxdepth: 1
   :hidden:

   mechanical_containers

.. toctree::
   :caption: System solvers
   :maxdepth: 1
   :hidden:

   system_solvers

.. toctree::
   :caption: Linear solvers
   :maxdepth: 1
   :hidden:

   linear_solvers

.. toctree::
   :caption: Topology containers
   :maxdepth: 1
   :hidden:

   topology_containers

.. toctree::
   :caption: Selectors
   :maxdepth: 1
   :hidden:

   selectors

.. toctree::
   :caption: Force fields
   :maxdepth: 1
   :hidden:

   forcefields

.. toctree::
   :caption: Mappings
   :maxdepth: 1
   :hidden:

   mapping

.. toctree::
   :caption: Collisions
   :maxdepth: 1
   :hidden:

   collisions

.. toctree::
   :caption: Contraints
   :maxdepth: 1
   :hidden:

   constraints

.. toctree::
   :caption: Mesh loaders
   :maxdepth: 1
   :hidden:

   mesh_loaders