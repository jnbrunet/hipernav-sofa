.. _constraints:

<FixedConstraint /\>
====================

Fix the positions and velocities of the given DOFs indices.

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - indices
      - 0 1 2 ...
      - The indices of the nodes to be fixed.

.. code-block:: xml
	:linenos:

	<MeshObjLoader name="loader" rotation="60 45 0" translation="5 4 0" filename="liver.obj" />
	<TriangleSetTopologyContainer src="@loader" />
	<MechanicalObject />

<MeshGmshLoader /\>