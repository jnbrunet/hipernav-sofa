 .. _mechanical_containers:

<MechanicalObject /\>
=====================

Contains vectors of positions, velocities and forces which will be updated in time by a system solver.

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `initial_position`
      - x0 x1 x2 … xn
      - Initialize the initial position vector with the specified values. While the positions vector will be updated by a system solver, the initial_position will stay the same for the entire simulation. 
    * - `position`
      - x0 x1 x2 … xn
      - Initialize the position vector with the specified values. This vector will be updated by the system solver with the new positions once the external forces and constraints are applied.
    * - `rest_position`
      - x0 x1 x2 … xn
      - Initialize the rest position vector with the specified values (by default, the initial_position vector will be used. While the positions vector will be updated by a system solver, the rest_position will usually stay the same as the rest (undeformed) state of the simulated object. 
    * - `translation`
      - tx ty tz
      - Apply the given translation vector to every nodes of the mechanical object.
    * - `rotation`
      - rx ry rz
      - Apply the given rotation vector to every nodes of the mechanical object.
    * - `showObject`
      - bool
      - Whether or not to display small sphere at the location of each nodes.
    * - `src`
      - path
      - Path to a mesh component or another mechanical object from which the vectors will be automatically copied.
    * - `template`
      - “Rigid” or “Vec3d”
      - Select the DOFs type where 
          - Rigid is (x y z qx qy qz qw)
          - Vec3d is (x y z)


.. code-block:: xml
	:linenos:

	<MechanicalObject 
	     template="Rigid" 
	     name="particles" 
	     position="0 0 0    0 0 0 1       0 1 0   0 0 0 1" 
	     translation="0 1 0" 
	     rotation="0 0 0" 
	     showObject="1" 
	/>

<OglModel /\>
=============

Visual representation of a topology. The OglModel contains a vector of positions, 

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - position
      - x0 x1 x2 … xn
      - Initialize the position vector with the specified values. Here, “xi” represent the position of node  i.
    * - color
      - r g b a
      - Set the color of the material shown. Here, r, g, b and a represent respectivaly red, green, blue and alpha each having a value between 0 and 255.
    * - translation
      - tx ty tz
      - Apply the given translation vector to every nodes.
    * - rotation
      - rx ry rz
      - Apply the given rotation vector to every nodes.
    * - src
      - path
      - Path to a mesh component or a mechanical object from which the positions vector will be automatically copied.
    * - normal
      - n0 n1 n2 ... nn
      - Vector containing the normal at each positions.
    * - edges
      - 0 1 2 3 ...
      - Node indices relative to the position vector where two consecutive nodes form an edge.
    * - triangles
      - 0 1 2 3 ...
      - Node indices relative to the position vector where three consecutive nodes form a triangle.
    * - quads
      - 0 1 2 3 ...
      - Node indices relative to the position vector where four consecutive nodes form a quad.
    * - filename
      - file path
      - Populate the positions, edges, triangles and quads from a given mesh file (ex. obj file).


.. code-block:: xml
	:linenos:

	<OglModel
	     name="visual" 
	     position="@mo.position" 
	     filename="liver.obj"
	/>



