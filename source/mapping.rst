 .. _mappings:

 .. role:: important

The mapping system allows to transfer the forces computed on a topology (the "input" elements) to another (usually underlying) topology (the "output" elements). Once a system solver computes the new positions of the "output" topology, the mapping will update the "input" topology from these.

<BarycentricMapping /\>
=======================
Map an input mechanical object embedded inside an output topology. The number and positions of nodes in the input mechanical object are allows to not match the number and positions of the output mechanical object. However, each of the input nodes must be entirely contained inside at least one element of the output topology.

:important:`Requires an input mechanical object.`
:important:`Requires an output mechanical object.`
:important:`Requires an output topology.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - input
      - path
      - Input mechanical objects that contains the positions mapped from the output elements. The barycentric coordinates of the input positions from within an output element will be used.
    * - output
      - path
      - Output mechanical objects that contains the positions of the elements embedding the input positions.


<IdentityMapping /\>
=======================
Map an input mechanical object that match exactly an output mechanical object (no topology required). The number and positions of nodes in the input mechanical object must match the number and positions of the output mechanical object at the beginning of the simulation.

:important:`Requires an input mechanical object.`
:important:`Requires an output mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - input
      - path
      - Input mechanical objects that contains the positions that will follow the output positions.
    * - output
      - path
      - Output mechanical objects that will receive the forces applied to the input positions.

<SubsetMapping /\>
=======================
Similar to the identity mapping, but here the input position vector can contain less nodes than the output position vectors. However, each nodes in the input mechanical object must share a node in the output mechanical object that have exactly the same position AND the same node indice.

:important:`Requires an input mechanical object.`
:important:`Requires an output mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - input
      - path
      - Input mechanical objects that contains the positions that will follow the output positions.
    * - output
      - path
      - Output mechanical objects that will receive the forces applied to the input positions.