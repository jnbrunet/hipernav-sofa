  .. _forcefields:

 .. role:: important

<ConstantForceField /\>
=======================
Apply constant force :math:`F = f_c` to given degrees of freedom.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - indices
      - 0 1 2 ...
      - Indices of the nodes contained in the mechanical object where the forces are applied.
    * - force
      - float
      - Applied force to all points.
    * - totalForce
      - float
      - Total force for all points, will be distributed uniformly over 

<SpringForceField /\>
=======================
Apply spring force :math:`F = k * ||x_1 - x_2||`  between pair of nodes from two different mechanical objects.

:important:`Requires two mechanical objects.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - stiffness
      - float
      - Stiffness actor :math:`k` applied between two points.
    * - object1
      - path
      - Path to the first mechanical object
    * - object2
      - path
      - Path to the second mechanical object

<TetrahedronFEMForceField /\>
=============================
Apply an elastic force :math:`F = Ku` from the computed linear strain on a set of tetrahedrons. Here the stiffness matrix :math:`K` is precomputed from the poisson ratio and the young modulus specified and :math:`u` is the displacement vector
computed between the current position vector and the initial position vector of the mechanical object.

:important:`Requires a mechanical object.`
:important:`Requires a tetrahedron set topology container.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - poissonRatio
      - float (between 0 and 0.49)
      - Poisson ratio which represent the level of imcompressibility of the material.
    * - youngModulus
      - float
      - Measure of the ability of a material to withstand changes in length when under lengthwise tension or compression.
    * - method
      - "small" or "large"
      - Use a corotated formulation of the element stress. The small method doesn't remove the rotation from the strain. The large method removes it by extracting it from the current state of the element.

<HexahedronFEMForceField /\>
============================
Apply an elastic force :math:`F = Ku` from the computed linear strain on a set of hexahedrons. Here the stiffness matrix :math:`K` is precomputed from the poisson ratio and the young modulus specified and :math:`u` is the displacement vector
computed between the current position vector and the initial position vector of the mechanical object.

:important:`Requires a mechanical object.`
:important:`Requires a hexahedron set topology container.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - poissonRatio
      - float (between 0 and 0.49)
      - Poisson ratio which represent the level of imcompressibility of the material.
    * - youngModulus
      - float
      - Measure of the ability of a material to withstand changes in length when under lengthwise tension or compression.
    * - method
      - "small" or "large"
      - Use a corotated formulation of the element stress. The small method doesn't remove the rotation from the strain. The large method removes it by extracting it from the current state of the element.