 .. role:: important

Topology containers contain the elements of the system (an element is an array of node indices). A topology does not contain the positions vector by itself, instead it in contains the indices of the node relative to the mechanical object vector positions.

<PointSetTopologyContainer /\>
==============================
Contains a set of point indices.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `points`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object.
    * - `src`
      - path
      - Path to another topology container from which this topology will import its indices.

.. code-block:: xml
  :linenos:

  <!-- Container of 6 points -->
  <MechanicalObject  template="Vec3d" position="0 0 0    0 1 0    1 0 0    1 0 0    1 1 0    0 1 0" showObject="1" />
  <PointSetTopologyContainer points="0 1 2 3 4 5" />

<TriangleSetTopologyContainer /\>
=================================
Contains a set of triangle indices.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `points`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object.
    * - `triangles`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object. Three consecutive indices will form a triangle.
    * - `src`
      - path
      - Path to another topology container from which this topology will import its indices.

.. code-block:: xml
  :linenos:
  
  <!-- Container of 2 triangles -->
  <MechanicalObject  template="Vec3d" position="0 0 0    0 1 0    1 0 0    1 0 0    1 1 0    0 1 0" showObject="1" />
  <TriangleSetTopologyContainer triangles="3 4 5 0 1 2" />

<QuadSetTopologyContainer /\>
=============================
Contains a set of quad indices.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `points`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object.
    * - `quads`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object. Four consecutive indices will form a quad.
    * - `src`
      - path
      - Path to another topology container from which this topology will import its indices.

.. code-block:: xml
  :linenos:
  
  <!-- Container of 1 quads -->
  <MechanicalObject  template="Vec3d" position="0 1 0    0 0 0    1 0 0    1 1 0" showObject="1" />
  <QuadSetTopologyContainer quads="1 0 3 2" />

<TetrahedronSetTopologyContainer /\>
====================================
Contains a set of tetrahedrons indices.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `points`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object.
    * - `tetras`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object. Four consecutive indices will form a tetrahedron.
    * - `src`
      - path
      - Path to another topology container from which this topology will import its indices.

.. code-block:: xml
  :linenos:
  
  <!-- Container of 1 tetrahedron -->
  <MechanicalObject  template="Vec3d" position="0 1 0    0 0 0    1 0 0    0.3 0.3 1" showObject="1" />
  <TetrahedronSetTopologyContainer tetras="1 0 2 3" />

<HexahedronSetTopologyContainer /\>
===================================
Contains a set of hexahedrons indices.

:important:`Requires a mechanical object.`

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - `points`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object.
    * - `hexas`
      - [0 1 2 ...]
      - List of point indices relative to the positions vector contained in the mechanical object. Eight consecutive indices will form an hexahedron.
    * - `src`
      - path
      - Path to another topology container from which this topology will import its indices.

.. code-block:: xml
  :linenos:
  
  <!-- Container of 1 hexahedron -->
  <MechanicalObject  template="Vec3d" position="0 0 0    1 0 0    1 1 0    0 1 0    0 0 1    1 0 1    1 1 1    0 1 1" showObject="1" />
  <HexahedronSetTopologyContainer hexas="0 1 2 3 4 5 6 7" />
