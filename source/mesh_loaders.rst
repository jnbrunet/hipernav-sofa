.. _mesh_loaders:

<MeshObjLoader /\>
=====================

Load a surface mesh (extension ".obj").

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - translation
      - tx ty tz
      - Apply the given translation vector to every nodes of the mesh.
    * - rotation
      - rx ry rz
      - Apply the given rotation vector to every nodes of the mesh.
    * - scale3d
      - sx sy sz
      - Apply the given scale factor to every nodes of the mesh.
    * - filename
      - File path
      - File path to a ".obj" file.

.. code-block:: xml
	:linenos:

	<MeshObjLoader name="loader" rotation="60 45 0" translation="5 4 0" filename="liver.obj" />
	<TriangleSetTopologyContainer src="@loader" />
	<MechanicalObject />

<MeshGmshLoader /\>
=====================

Load a volumetric mesh from a file (extension ".msh").

.. list-table::
    :widths: 10 10 80
    :header-rows: 1
    :stub-columns: 0

    * - Name
      - Format
      - Description
    * - translation
      - tx ty tz
      - Apply the given translation vector to every nodes of the mesh.
    * - rotation
      - rx ry rz
      - Apply the given rotation vector to every nodes of the mesh.
    * - scale3d
      - sx sy sz
      - Apply the given scale factor to every nodes of the mesh.
    * - filename
      - File path
      - File path to a ".obj" file.

.. code-block:: xml
	:linenos:

	<MeshGmshLoader name="loader" rotation="60 45 0" translation="5 4 0" filename="liver.msh" />
	<TetrahedronSetTopologyContainer src="@loader" />
	<MechanicalObject />